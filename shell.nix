with (import <nixpkgs> {}).pkgs;
let pkg = haskellngPackages.callPackage
            ({ mkDerivation, amqp, base, mime-mail, network
             , optparse-applicative, pipes, pipes-concurrency, pipes-safe
             , smtp-mail, stdenv, text
             }:
             mkDerivation {
               pname = "amqp-mailer";
               version = "0.1.0.0";
               src = ./.;
               isLibrary = false;
               isExecutable = true;
               buildDepends = [
                 amqp base mime-mail network optparse-applicative pipes
                 pipes-concurrency pipes-safe smtp-mail text
               ];
               description = "AMQP based mailer";
               license = stdenv.lib.licenses.unfree;
             }) {};
in
  pkg.env
