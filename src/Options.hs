module Options (Options (..), options) where

import Options.Applicative (
	Parser,
	help, long, metavar, short, strOption, value,
	(<>))

data Options = Options {
	host :: String,
	path :: String,
	username :: String,
	password :: String,
	queue :: String}

options :: Parser Options
options = Options
	<$> strOption (
		long "host"
		<> short 'h'
		<> value "127.0.0.1"
		<> metavar "HOST"
		<> help "Host of the AMQP server")
	<*> strOption (
		long "path"
		<> short 'p'
		<> value "/"
		<> metavar "PATH"
		<> help "Path of the AMQP server")
	<*> strOption (
		long "username"
		<> short 'u'
		<> value "guest"
		<> metavar "USERNAME"
		<> help "Username on the AMQP server")
	<*> strOption (
		long "password"
		<> short 'p'
		<> value "guest"
		<> metavar "PASSWORD"
		<> help "Password on the AMQP server")
	<*> strOption (
		long "queue"
		<> short 'q'
		<> value "queue"
		<> metavar "QUEUE"
		<> help "Queue name on the AMQP server")
