{-# LANGUAGE OverloadedStrings, RecordWildCards #-}

module Main (main) where

import Data.Text (pack)
import Data.Text.Lazy.Encoding (decodeUtf8)
import Network.AMQP (msgBody)
import Network.Mail.SMTP (Address (Address), plainTextPart)
import Options (Options (..), options)
import Options.Applicative (
	execParser, fullDesc, helper, info, progDesc,
	(<>))
import Pipes (runEffect, (>->))
import Pipes.Prelude (map)
import Pipes.Safe (runSafeT)
import Prelude hiding (map)
import Proxy (amqpD, mailerD)


main :: IO ()
main = execParser opts >>= application
	where
	opts = info (helper <*> options)
		(fullDesc <> progDesc "Print AMQP messages")
	
application :: Options -> IO ()
application Options{..} =
	runSafeT . runEffect $
		amqpD
			host
			(pack path)
			(pack username)
			(pack password)
			(pack queue)
		>-> map (return . plainTextPart . decodeUtf8 . msgBody . fst)
		>-> mailerD
			"localhost"
			(Address Nothing "noreply@julienlanglois.me")
			[Address (Just "Julien Langlois") "yourstruly@julienlanglois.me"]
			"Test"
