-- |Proxy definitions for use in building an AMQP based session.
module Proxy (amqpD, mailerD) where

import Control.Monad (forever, void)
import Pipes (Consumer, MonadIO, Producer, await, lift, liftIO)
import Pipes.Concurrent (
	atomically, bounded, forkIO, fromInput, send, spawn)
import Pipes.Safe (SafeT, bracket)
import Data.Text (Text)
import Network.AMQP (
	Ack (NoAck), Envelope, Message,
	closeConnection, consumeMsgs, openChannel, openConnection)
import Network.Mail.Mime (Part)
import Network.Mail.SMTP (Address, sendMail, simpleMail)
import Network.Socket (HostName)

-- |AMQP producer proxy.
amqpD :: String -- ^ AMQP host
	-> Text -- ^ AMQP virtual host
	-> Text -- ^ AMQP username
	-> Text -- ^ AMQP password
	-> Text -- ^ Queue name
	-> Producer (Message, Envelope) (SafeT IO) ()
amqpD host path username password queue = bracket
	(do
		connection <- openConnection host path username password
		channel <- openChannel connection
		return (connection, channel))
	(\(connection, _) -> closeConnection connection)
	(\(_, channel) -> do
		(output, input) <- lift . lift $ spawn (bounded 1)
		void . lift . lift . forkIO . void . consumeMsgs channel queue NoAck $ \m ->
			void . atomically $ send output m
		fromInput input)

-- |Mailer downstream proxy
mailerD :: (MonadIO m)
	=> HostName -- ^ SMTP server hostname
	-> Address -- ^ Sender
	-> [Address] -- ^ Receivers
	-> Text -- ^ Subject line
	-> Consumer [Part] m ()
mailerD hostname from to subject = forever $ do
	contents <- await
	liftIO . sendMail hostname $ simpleMail from to [] [] subject contents
